<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link href='http://fonts.googleapis.com/css?family=Roboto:100,300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/style-responsive.css" />
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/modernizr.js"></script>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="mp-pusher" class="mp-pusher">
    <nav class="mp-menu" id="mp-menu">
		<div class="mp-level">    	
	        <?php
	           	wp_nav_menu( 
	            	array( 
	            		'menu' => 'primary',
	            		'container'  => 'false',
	            		'container_id' => 'mp-menu',
	            		'menu_class' => 'mp-level',
	            		'container_class' => 'mp-menu',
	            		'menu_class' => '',  
	            		'walker' => new SBM_Responsive_Nav()
	            	) 
	            ); 
	        ?>	
                    
        </div>
    </nav>

    <header>
        <div class="container">
            <div class="row">
                <div class="col-sm-6" id="logo">
                <a href="<?php echo get_home_url(); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/flex_towel_logo.png" width="123" height="60"  alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"/>
                </a>
                </div>
                <div class="col-sm-6 menu-header"><a href="#" id="trigger" class="menu-trigger"></a></div>
            </div>
        </div>
    </header>