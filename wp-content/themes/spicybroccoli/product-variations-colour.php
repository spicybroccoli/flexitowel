<?php
// THIS PAGE SHOWS ONLY THE AVAILABLE COLOR OPTIONS OF THE PRODUCT
?>
Please select the colour of the towel
		<ul class="variations_colors_panel" id="towel_color">
			<?php 
			$already_there = array(); //DECLARE AS ARRAY TO NOT GET ERROR ON IN_ARRAY FUNCTION
			$variations = $_product->get_available_variations(); //get_variation_attributes
				foreach($variations as $variation){
					// CHECK IF THE COLOR IS NOT ALREADY DISPLAYED
					if(!in_array($variation['attributes']['attribute_pa_colour'], $already_there)){
						//CHECK IF THE VARIABLE PASSED IS THE SAME AS CURRENT - TO CHECK IT
						if($_REQUEST[ 'attribute_pa_colour'] == $variation['attributes']['attribute_pa_colour']) {
							$selected =  ' selected';
						}else{ $selected = '';}
					?>
                
					<li >
                        <a 	rel="<?php echo $variation['image_src'] ?>" 
                        	id="<?php echo $variation['variation_id'] ?>" 
                            class="<?php echo $variation['attributes']['attribute_pa_colour'] ?><?php echo $selected; ?>">
                        </a></li>
                        		
				<?php 
					}
				//INSERT THE COLOUR IN AN ARRAY TO CHECK
				//---	
				$already_there[] = $variation['attributes']['attribute_pa_colour'];
				}
			?>
            
            </ul>