<?php get_header(); ?>
<section id="page">
  <div class="container">
  <?php if ( have_posts() ) : the_post(); ?>
    <article class="<?php 
    global $post;
    echo $post->post_name;
?>">
              
              <div class="header">
                  <h1><?php the_title(); ?></h1>
                  <?php 
                  //GET SUB TITLE IF EXISTS
                  $sub_title = get_post_meta(get_the_ID(),'title_text_description',true);
                  if($sub_title) :
                  ?>
                    <h2><?php echo $sub_title; ?></h2>
                  <?php endif; ?>
              </div>
              <?php the_content(); ?>
              
    </article>
	<?php endif; ?>
  </div>
</section>
<?php get_footer(); ?>