// JavaScript Document
jQuery(document).ready(function($) {
	
	//ADD MONOGRAM
	// SELECT MONOGRAM FIELD BY CLICK ON CKECK BOX
	jQuery('[name="checkboxMonogram"]').click(function(){
		checkCheckBox( jQuery( this ) );
	});
	
	function checkCheckBox( element ){
		var check_val; 
		
		if(jQuery( element ).is(':checked')){
			check_val = 'yes';
			jQuery('div.monogram-over').addClass('hidden');
		}else{
			check_val = 'no';
			jQuery('div.monogram-over').removeClass('hidden');
		}
		elem = jQuery('[name="attribute_pa_monogram-your-towel"]');
		elem.val( check_val ).change();
	}
	checkCheckBox( jQuery( '[name="checkboxMonogram"]' ) ); //CALL FUNCTION ON LOAD
	
	// MONOGRAM LETTER
	jQuery('[name="monogram-letter-type"]').bind('keyup blur',function(){ 
		jQuery('[name="monogram-letter-type"]').val(function(i, val) {
			
			if (/[a-z\s]/gi.test(val)){
				jQuery('.added').remove();
				value = val.toLowerCase();
				jQuery('[name="attribute_pa_letter"]').val( value );
				return value; 
			}else{
				//IF ALREADY EXISTS THE DIV DO NOT PUT AGAIN
				if( jQuery('.added').length == 0 ){
					jQuery( '[name="monogram-letter-type"]' ).after('<div class="added">Must type a letter A-Z</div>');
					jQuery('[name="attribute_pa_letter"]').val( 'none' )
				}
			}
    	});
	});
	
	
	/* CHANGE HOME PAGE IMAGE BY CLICK ON COLOR*/
	jQuery('.home ul#towel_color li a').click(function(e){
		e.preventDefault();
		jQuery('#img-product-home').attr('src', jQuery( this ).attr('rel'));
		jQuery('[name="attribute_pa_colour"]').val( jQuery( this ).attr('class') );
	});
	
	/* 
	TOWEL COLOR
	CHANGE PRODUCT COLOR ON PRODUCT PAGE BY CLICK ON COLOR*/
	jQuery('ul#towel_color li a').click(function(e){
		e.preventDefault();
		//CHECK EVERY LIST IF HAS A SELECTED CLASS TO UNSELECT
		jQuery('ul#towel_color li a').each(function(index, element) {
            if(jQuery( this ).hasClass('selected')){ 
				jQuery( this ).removeClass('selected');
			}
        });
		//CHANGE PROD LIST
		jQuery('[name="attribute_pa_colour"]').val( jQuery( this ).attr('class') ).change();
		// INSERT SELECT CLASS TO THE CURRENT CLICKED COLOR
		jQuery( this ).addClass('selected');
	});
	
	/* MONOGRAM COLOR 
	CHANGE PRODUCT COLOR ON PRODUCT PAGE BY CLICK ON COLOR*/
	jQuery('.single-product ul#monogram_color li a').click(function(e){
		e.preventDefault();
		//CHECK EVERY LIST IF HAS A SELECTED CLASS TO UNSELECT
		jQuery('ul#monogram_color li a').each(function(index, element) {
            if(jQuery( this ).hasClass('selected')){ 
				jQuery( this ).removeClass('selected');
			}
        });
		//CHANGE PROD LIST
		jQuery('[name="attribute_pa_monogram-color"]').val( jQuery( this ).attr('class') ).change();
		// INSERT SELECT CLASS TO THE CURRENT CLICKED COLOR
		jQuery( this ).addClass('selected');
	});
	
	/* MONOGRAM FONT 
	CHANGE PRODUCT COLOR ON PRODUCT PAGE BY CLICK ON COLOR*/
	jQuery('.single-product ul#font_selector li a').click(function(e){
		e.preventDefault();
		//CHECK EVERY LIST IF HAS A SELECTED CLASS TO UNSELECT
		jQuery('ul#font_selector li a').each(function(index, element) {
            if(jQuery( this ).hasClass('selected')){ 
				jQuery( this ).removeClass('selected');
			}
        });
		//CHANGE PROD LIST
		jQuery('[name="attribute_pa_monogram-font"]').val( jQuery( this ).attr('class') ).change();
		// INSERT SELECT CLASS TO THE CURRENT CLICKED COLOR
		jQuery( this ).addClass('selected');
	});
	
	
	
	/* INCREASE QUANTITY BY BUTTONS */
		jQuery("#qtyplus").click(function(){
		  jQuery('input[name="quantity"]').val( Number(jQuery('input[name="quantity"]').val()) + 1 );
		});
	  jQuery("#qtyminus").click(function(){
		  if($('input[name="quantity"]').val() > 1){
			   $('input[name="quantity"]').val( Number(jQuery('input[name="quantity"]').val()) - 1 );
		  }
		});
		
		/* ADD TO CART*/
	jQuery('#add-custom-cart').click(function(e){
		console.log('Add cart clicked');
		e.preventDefault();
		jQuery('form.variations_form.cart').submit();
	});
	
	jQuery('#see-prod-submit').click(function(e){
		e.preventDefault();
		jQuery('form#select-color-form').submit();
	});
	
	/** SCROLLER */
	jQuery('nav #menu-front-page-menu a').click(function(){
		console.log(jQuery.attr(this, 'href'));
		jQuery('html, body').animate({
			scrollTop: jQuery( jQuery.attr(this, 'href') ).offset().top
		}, 700);
		return false;
	});
});