<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
// Include local configuration
if (file_exists(dirname(__FILE__) . '/local-config.php')) {
	include(dirname(__FILE__) . '/local-config.php');
}

// Global DB config
if (!defined('DB_NAME')) {
	define('DB_NAME', 'db142268_flexitowel');
}
if (!defined('DB_USER')) {
	define('DB_USER', 'db142268');
}
if (!defined('DB_PASSWORD')) {
	define('DB_PASSWORD', 'fingered!');
}
if (!defined('DB_HOST')) {
	define('DB_HOST', 'internal-db.s142268.gridserver.com');
}

/** Database Charset to use in creating database tables. */
if (!defined('DB_CHARSET')) {
	define('DB_CHARSET', 'utf8');
}

/** The Database Collate type. Don't change this if in doubt. */
if (!defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0#T2:9qCk#$xSpo,?-omnGeJdMBjlb/e)>7c12-I4AhW:b2$^:(N3?G=I`>u}G~{');
define('SECURE_AUTH_KEY',  '+2F3G+5Ku:PoWdO+GA,H:DH?5k+lA|iey33[W^-Ha]{j/K.^$r)M&DrHpi^ZOn|)');
define('LOGGED_IN_KEY',    'i25SS.Az7PfDqC=v`s&EZAN#x=^wg8QF+-^V5y) 2%%:B,|9M.FXWUl1!kRHzu+T');
define('NONCE_KEY',        'BK;fVz;-V]r/ZPX-eD2[>/{c4tKdX&+U=EwLs]|mV3OeDzL[s~ZmF~@jVA`p3}w=');
define('AUTH_SALT',        'a(k`iQGpyhADrm~,zK|/n=}G)wH>45*GR.7u3q[p,D+3*,9FOb$V2[-*0pt<PvfO');
define('SECURE_AUTH_SALT', 'd2l^}9JZvZ}Ru:dUVV<n&+|!+Gs=2>2kvm70xx|@F; B9.T+U&|Dj}x*~7=n+%(4');
define('LOGGED_IN_SALT',   'Hj~)zOFLuMA.v-. tmmxR5*sFAp2qER|>)x;xp1@_`1r~]u;0S*qWfU[ MmgGr6E');
define('NONCE_SALT',       'hWq%q(=m^&f_V+~I:egR>7#5DN5||-UH%l~yQAL+u]-KZ8;u]+b-#yOV]-X%$yFQ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sbm_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');



/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if (!defined('WP_DEBUG')) {
	define('WP_DEBUG', false);
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
