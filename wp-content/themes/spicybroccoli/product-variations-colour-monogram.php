<?php
// THIS PAGE SHOWS ONLY THE AVAILABLE MONOGRAM COLOR OPTIONS OF THE PRODUCT
?>
		<ul class="variations_colors_panel" id="monogram_color">
            <?php 
			// SLUGS INSERTED IN HERE WILL NOT BE SHOWED ON PRODUCT
			// 'NONE' MUST BE IN BECAUSE BECAUSE IS THE DEFAULT 
			$no_show_slug = array('none');
			$variations = get_terms("pa_monogram-color");
				foreach($variations as $variation){ 
				//CHECK IF VARIATION SLUG WILL SHOW OR NOT
				if(!in_array($variation->slug, $no_show_slug)){
					//CHECK IF THE VARIABLE PASSED IS THE SAME AS CURRENT - TO CHECK IT
						if($_REQUEST[ 'attribute_pa_monogram-color'] == $variation->slug) {
							$selected =  ' selected';
						}else{ $selected = '';}
				?>
					<li >
                        <a class="<?php echo $variation->slug ?><?php echo $selected; ?>" var-name="<?php echo $variation->name ?>" var-id="<?php echo $variation->term_id ?>"></a></li>		
				<?php }
				}
			?>
            </ul>