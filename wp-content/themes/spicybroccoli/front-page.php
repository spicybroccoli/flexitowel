<?php get_header(); ?>
<section id="slider">
	<?php echo do_shortcode("[metaslider id=72]"); ?>
</section>
<section id="sub-nav">
	<div class="container">
	<nav>
    	<?php
	           	wp_nav_menu( 
	            	array( 
	            		'theme_location' => 'frontpage_menu',
	            		'container'  => 'false'
	            	) 
	            ); 
	        ?>	
    </nav>
    </div>
</section>
<section id="hero">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/hero_img.jpg" class="img-responsive img-center"/>
</section>
<section id="mrs">
<?php $post = get_post(84); // APPLICATION PAGE ?>
	<div class="container">
    	<div class="col-two left">
         <?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
   	    <img src="<?php echo $feat_image; ?>" class="img-responsive" alt="<?php echo $post->post_title; ?>"/> </div>
    	<div class="col-two right">
        	<?php echo apply_filters("the_content", $post->post_content); //getLimitedText($post->post_content, 520, false); ?>
            <a href="<?php the_permalink(); ?>">› Read More</a>
	  </div>
    </div>
</section>
<section id="feature">
	<div class="container">
    	<div class="row">
        <?php //MAX CHARACTERES ON SHOW CONTENT FOR NEXT 03 FEATURES
				$max_char = 250; ?>
        <?php $post = get_post(77); ?>
        	<div class="col-size-4"><h2><?php echo $post->post_title; ?></h2>
            <p><?php echo getLimitedText($post->post_content, $max_char, false); ?>  </p>
            <a href="<?php the_permalink(); ?>" title="<?php echo $post->post_title; ?>">› Read More</a></div>
        <?php $post = get_post(79); ?>
            <div class="col-size-4 center"><h2><?php echo $post->post_title; ?></h2>
            <p><?php echo getLimitedText($post->post_content, $max_char, false); ?>  </p>
            <a href="<?php the_permalink(); ?>" title="<?php echo $post->post_title; ?>">› Read More</a></div>
        <?php $post = get_post(81); ?>
            <div class="col-size-4"><h2><?php echo $post->post_title; ?></h2>
            <p><?php echo getLimitedText($post->post_content, $max_char, false); ?>  </p>
            <a href="<?php the_permalink(); ?>" title="<?php echo $post->post_title; ?>">› Read More</a></div>
        </div>
    </div>
</section>
<section id="application">

<?php $post = get_post(74); // MRS & MS PAGE ?>
	<div class="container">
    	<div class="col-two left">
        <?php echo apply_filters("the_content", $post->post_content); //getLimitedText($post->post_content, 520, false); ?>
            <a href="<?php the_permalink(); ?>">› Read More</a>
        </div>
      <div class="col-two right">
       <?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
   	    <img src="<?php echo $feat_image; ?>" class="img-responsive" alt="<?php echo $post->post_title; ?>"/> </div>
    </div>
</section>
<section id="product_sec">
	<?php
	// GETTING PRODUCT OBJECT
	$_pf = new WC_Product_Factory();  
    $_product = $_pf->get_product( 13 );
    // from here $_product will be a fully functional WC Product object, 
    // you can use all functions as listed in their api
	// http://docs.woothemes.com/wc-apidocs/class-WC_Product.html
				?>
	<div class="product_sec_img">
    <div class="container">
    <h1>This is your<br><span>flexitowel</span></h1>
    <?php echo $_product->get_image('full', array('class' => 'img-responsive','id' => 'img-product-home')); ?>
         
    </div>
    </div>
    <div class="product_sec_details">
	<div class="container">
    <div class="row">
    <a id="colours"></a>
    <a id="buy"></a>
        	<div class="col-size-6">
            <h2>Product Details</h2>
            <p><?php echo apply_filters("the_content", $_product->post->post_content); //$_product->post->post_content; ?></p>
            </div>
            <form id="select-color-form" action="http://dev.spicybroccolitesting.com.au/flexitowel/product/flextowel-02/" method="GET">
            <div class="col-size-6"><h4>Choose your colour</h4>
             <?php include('product-variations-colour.php'); ?>
             <input type="hidden" name="attribute_pa_colour" value="">
             <a class="btn btn-black" id="see-prod-submit" href="#">See Product</a>
             </div>
             </form>
             </div>
    </div>
    </div>
</section>
<?php get_footer(); ?>