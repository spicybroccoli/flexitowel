<?php
/*
		THIS PAGE IS CALLED FROM spicybroccoly/WOOCOMMERCE/content-single-product.php INSIDE THIS THEME.
*/
$shows_in = array(13); //INSERT HERE THE ID OF PRODUCTS WHERE MONOGRAM WILL BE SHOWED

?>
<?php
	// GETTING PRODUCT OBJECT
	$_pf = new WC_Product_Factory();  
    $_product = $_pf->get_product( );
    // from here $_product will be a fully functional WC Product object, 
    // you can use all functions as listed in their api
	// http://docs.woothemes.com/wc-apidocs/class-WC_Product.html
				?>
<section id="product-features">
    <div class="col-two left">
         <h3>Select Color</h3>
         <?php include('product-variations-colour.php'); ?>
         <?php if(in_array(get_the_ID(), $shows_in)): ?>
         <h3>Monogram your towel</h3>
         <?php
		 //CHECK IF THE VARIABLE PASSED IS THE SAME AS YES - TO CHECK IT
				if($_REQUEST[ 'attribute_pa_monogram-your-towel'] == 'yes') {
					$checked =  ' checked';
				}else{ $checked = '';}
		 ?>
         <input type="checkbox" name="checkboxMonogram" id="checkbox"<?php echo $checked; ?>><label for="checkbox">Add a monogram </label>
         <?php endif; ?>
         <h3>Quantity</h3>
         <button id="qtyminus" onclick="return false">-</button>
         <input type="number" step="1" name="quantity" value="1" max="3" readonly min="1">
		<button id="qtyplus" onclick="return false">+</button>
     </div>
     
     <div class="col-two right" >
     <?php if(in_array(get_the_ID(), $shows_in)): ?>
     <div class="monogram-over">
         <h3>Monogram Letter</h3>
         Please type a letter into the box bellow
         <input type="text" name="monogram-letter-type" maxlength="1" value="<?php echo $_REQUEST[ 'attribute_pa_letter'] ; ?>">

         <h3>Monogram Colour</h3>
         Please select the colour of the monogram
         <?php include('product-variations-colour-monogram.php'); ?>
         
         <h3>Monogram Font</h3>
         Please select the font
         <?php include('product-variations-font-monogram.php'); ?>
     </div>
     <?php endif; ?>
     <a class="btn btn-black" id="add-custom-cart" href="#">Add to Cart</a>
     </div>
     
</section>