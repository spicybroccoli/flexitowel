<?php 
//	Shows if the page is not the id 8
if(get_the_ID() != 8): ?>
<section id="product-bottom">
<div class="container">
<?php
	// GETTING PRODUCT OBJECT
	$_pf = new WC_Product_Factory();  
    $_product = $_pf->get_product( 8 );
    // from here $_product will be a fully functional WC Product object, 
    // you can use all functions as listed in their api
	// http://docs.woothemes.com/wc-apidocs/class-WC_Product.html
				?>
    <div class="row">
    <div class="col-size-5">
         <h3>Related Product</h3>
         <p>Have you seen the Mr & Mrs suppa duppa awesome towels?</p>
         <a class="btn btn-green" href="<?php echo $_product->get_permalink( ); ?> ">See Product</a>
     </div>
         <div class="related-product-txt">
             Special <span>Order</span></div>
             <div class="related-product-image">
             <img src="http://dev.spicybroccolitesting.com.au/flexitowel/wp-content/uploads/2015/06/img_towel_msmrs.png"  alt=""/>
             </div>
    </div>
</div>
</section>
<?php endif; ?>