<footer>
<section id="footer-nav">
	<div class="container">
    	<nav>
    	<?php
	           	wp_nav_menu( 
	            	array( 
	            		'theme_location' => 'footer_menu',
	            		'container'  => 'false'
	            	) 
	            ); 
	        ?>
    </nav>
    </div>
</section>
<section id="copy">
	<div class="container">
    <div class="row">
    	<div class="col-md-6 col-sm-12 copyright">Copyright © 2015</div>
        <div class="col-md-6 col-sm-12 sbm"><a href="#">Spicy Broccoli Media</a></div>
        </div>
    </div>
</section>
</footer>

</div><!-- pusher -->

<?php wp_footer(); ?>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/app.js" ></script>

<script>	new mlPushMenu( document.getElementById( 'mp-menu' ), document.getElementById( 'trigger' ) );</script>

</body>

</html>

