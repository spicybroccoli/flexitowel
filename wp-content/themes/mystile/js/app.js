// JavaScript Document
jQuery(document).ready(function($) {
	
	jQuery('[name="attribute_pa_monogram-your-towel"]').addClass('hideForm');
	jQuery('[name="attribute_pa_monogram-your-towel"]')
		.before('<input type="checkbox" name="checkboxMonogram" id="checkbox"><label for="checkbox">Yes </label>');
	
	jQuery("option[value='']").each( function (){
		jQuery( this ).remove();
	});
	
	jQuery('#pa_colour').prepend('<option value="">Select a color</option>');
	
	jQuery('[name="checkboxMonogram"]').click(function(){
		
		var check_val; 
		
		if(jQuery( this ).is(':checked')){
			check_val = 'yes';
		}else{
			check_val = 'no';
		}
		console.log( check_val );
		jQuery('[name="attribute_pa_monogram-your-towel"]').val( check_val )
	});
});